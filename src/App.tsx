import { DashboardScreen } from "./screens/dashboard/dashboard.screen";
import logo from "./patient-logo.png";
import "./App.scss";
import { MedicalIcon } from "@fluentui/react-icons-mdl2";
import { Text } from "@fluentui/react";

function App() {
  return (
    <div className="App">
      <div className="header">
        <img style={{ height: 30 }} src={logo} />
        <div>
          <MedicalIcon />
          &nbsp;&nbsp;
          <Text className="doctor-name" variant="medium">Dr. Bones</Text>
        </div>
      </div>
      <DashboardScreen />
    </div>
  );
}

export default App;
