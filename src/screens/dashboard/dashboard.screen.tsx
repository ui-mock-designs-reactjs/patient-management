import React, { useState } from "react";
import { UserListing } from "../user-list/user-list.partial";
import { Stack } from "@fluentui/react";
import { UserDetails } from "../user-details/user-details";
import { IUser } from "../user-list/sample-data";
import "./dashboard.screen.scss";
import { AuxilaryData } from "../auxilary-data/auxilary-data.partial";

export const DashboardScreen = () => {
  const [selectedUser, setSelectedUser] = useState<IUser>();
  return (
    <Stack className="dashboard-screen" grow={1} horizontal>
      <UserListing selectedUser={selectedUser} setSelectedUser={setSelectedUser} />
      {selectedUser && <UserDetails user={selectedUser} />}
      {selectedUser && <AuxilaryData />}
    </Stack>
  );
};
