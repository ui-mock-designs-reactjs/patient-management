import React from "react";
import "./timeline.scss";
import { Timeline, TimelineEvent } from "react-event-timeline";
import {
  AddPhoneIcon,
  HospitalIcon,
  MedicalIcon,
  ReminderTimeIcon,
  SendIcon,
} from "@fluentui/react-icons-mdl2";
import { Stack, Text } from "@fluentui/react";

const timelineData = [
  {
    id: 1,
    createdAt: "2016-09-12 10:06 PM",
    title: "Appointment fixed",
    icon: "phone",
    iconColor: "#03a9f4",
  },
  {
    id: 2,
    createdAt: "2016-09-12 10:06 PM",
    title: "Physical examination completed",
    icon: "schedule",
    iconColor: "#607d8b",
  },
//   {
//     id: 3,
//     createdAt: "2016-09-12 10:06 PM",
//     title: "Blood sample taken for tests.",
//     icon: "bloodtype",
//     iconColor: "#e91e63",
//   },
  {
    id: 4,
    createdAt: "2016-09-12 10:06 PM",
    title: "Medication prescribed",
    icon: "medication",
    iconColor: "#009688",
  },
  {
    id: 5,
    createdAt: "2016-09-12 10:06 PM",
    title: "Bed rest suggested",
    icon: "single_bed",
    iconColor: "#00bcd4",
  },
];

export const TimelinePartial = () => {
  return (
    <div>
      <Stack className="timeline-container">
        <Text className="heading" variant="medium">
          Timeline
        </Text>
        <Timeline style={{paddingTop: 10}} lineColor="#456dfbc4" orientation="left">
          {timelineData.map((t) => (
            <TimelineEvent
              bubbleStyle={{ borderWidth: 2 }}
              titleStyle={{ fontSize: 16 }}
              subtitleStyle={{ color: "red" }}
              key={t.id}
              container="card"
              style={{
                border: "1px solid rgba(0,0,0,0.1)",
                borderRadius: 5,
                boxShadow: "none",
              }}
              cardHeaderStyle={{
                backgroundColor: "white",
                color: "#503331",
                borderRadius: 5,
              }}
              createdAt="2016-09-12 10:06 PM"
              title={t.title}
              icon={<span className="material-icons">{t.icon}</span>}
              iconColor={"#456dfbc4"}
            ></TimelineEvent>
          ))}
        </Timeline>
      </Stack>
    </div>
  );
};
