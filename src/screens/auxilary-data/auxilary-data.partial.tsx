import { Stack, Text } from "@fluentui/react";
import React from "react";
import * as ReactIcons from "@fluentui/react-icons-mdl2";
import { DefaultButton, PrimaryButton } from "@fluentui/react/lib/Button";
import "./auxilary-data.partial.scss";

const files = [
  { id: 1, name: "Check Up Result.pdf", size: 123 },
  { id: 2, name: "Dental X-Ray Result2.pdf", size: 451 },
  { id: 3, name: "Medical Prescriptions.pdf", size: 87 },
  { id: 4, name: "Dental X-Ray Result2.pdf", size: 346 },
  { id: 5, name: "Check Up Result.pdf", size: 123 },
  { id: 6, name: "Dental X-Ray Result2.pdf", size: 451 },
  { id: 7, name: "Medical Prescriptions.pdf", size: 87 },
  // { id: 8, name: "Dental X-Ray Result2.pdf", size: 346 },
  // { id: 9, name: "Check Up Result.pdf", size: 123 },
  // { id: 10, name: "Dental X-Ray Result2.pdf", size: 451 },
  // { id: 11, name: "Medical Prescriptions.pdf", size: 87 },
  // { id: 12, name: "Dental X-Ray Result2.pdf", size: 346 },
];

const notes = [
  {
    id: 1,
    name: "Initial checkup",
    text: "Sed ut perspiciatis unde omnis iste natus eos et accusamus",
    doctor: "Dr. Mega Nanade",
    date: "20 Nov '19",
  },
  {
    id: 2,
    name: "Follow-up details",
    text: "At vero eos et accusamus et iusto odio dignissimos ducimus",
    doctor: "Dr. Mega Nanade",
    date: "17 April '20",
  },
];

export const AuxilaryData = () => {
  return (
    <Stack className="auxilary-data">
      <Stack className="user-notes">
        <Stack horizontal horizontalAlign="space-between" className="header">
          <Text className="heading" variant="medium">
            Notes
          </Text>
          <Text variant="medium" className="all-notes-button">
            See all
          </Text>
        </Stack>

        <div className="note-container">
          <textarea spellCheck={false} placeholder="Enter notes here ..." className="user-note" />
          <PrimaryButton
            className="save-note-button"
            text="save"
            onClick={() => {}}
          />
        </div>
        <Stack className="notes-list">
          {notes.map((note) => (
            <>
              <Stack className="note-item">
                <Text className="note-text">{note.text}</Text>
                <Stack
                  horizontal
                  verticalAlign="center"
                  horizontalAlign="space-between"
                >
                  <Text variant="small" className="note-doctor">
                    <ReactIcons.MedicalIcon className="doctor-icon" />{" "}
                    {note.doctor}
                  </Text>
                  <Text variant="xSmall" className="note-date">
                    {note.date}
                  </Text>
                </Stack>
              </Stack>
            </>
          ))}
        </Stack>
      </Stack>
      <Stack className="user-files">
        <Stack horizontal horizontalAlign="space-between" className="header">
          <Text className="heading" variant="medium">
            Files/Documents
          </Text>
          <Stack horizontal verticalAlign="center">
            <ReactIcons.OpenFileIcon className="file-icon" />
            <Text variant="medium" className="all-notes-button">
              Add files
            </Text>
          </Stack>
        </Stack>
        <Stack grow={1} className="file-list">
          {files.map((file) => (
            <Stack
              verticalAlign="center"
              horizontalAlign="space-between"
              horizontal
              key={file.id}
              className="user-file"
            >
              <Stack horizontal verticalAlign="center">
                <ReactIcons.TextDocumentIcon
                  style={{ fontSize: 12, color: "gray", paddingRight: 3 }}
                />
                <Text variant="medium" style={{ color: "rgba(0,0,0,0.7)" }}>
                  {" "}
                  {file.name}
                </Text>
              </Stack>
              {file.id % 2 === 0 && (
                <div>
                  <ReactIcons.DeleteIcon className="file-item-icon" />
                  <ReactIcons.DownloadIcon className="file-item-icon" />
                </div>
              )}
              {file.id % 2 === 1 && (
                <Text className="file-size" variant="small">
                  {file.size}kb
                </Text>
              )}
            </Stack>
          ))}
        </Stack>
      </Stack>
    </Stack>
  );
};
