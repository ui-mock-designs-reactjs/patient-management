import React from "react";
import { PersonaSize, Stack, Text, VerticalDivider } from "@fluentui/react";
import { IUser } from "../user-list/sample-data";
import { UserListItem } from "../../components/user-list-item/user-list-item.component";
import "./user-details.scss";
import { SelectedUserCard } from "../../components/selected-user-card/selected-user-card";
import { SelectedUserBasicDetails } from "../../components/selected-user-basic-details/selected-user-basic-details";
import { DetailSelectorTabs } from "../../components/detail-selector-tabs/detail-selector-tabs";
import { TimelinePartial } from "../timeline/timeline";
import { Scans } from "../scans/scans";

interface Props {
  user: IUser;
}

export const UserDetails = (props: Props) => {
  return (
    <Stack grow={1} className="user-details">
      {/* <Stack.Item className="user-details-header">
        <UserListItem
          size={PersonaSize.size24}
          hidePresence={true}
          user={props.user}
        />
      </Stack.Item> */}
      <Stack horizontal className="user-details-body">
        <SelectedUserCard user={props.user} />
        <SelectedUserBasicDetails user={props.user} />
      </Stack>
      <Stack grow={1}>
        <Scans />
        <TimelinePartial />
      </Stack>
      {/* <Stack grow={1} className="second-row">
        <DetailSelectorTabs />
        <Stack.Item className="tab-details" grow={1}>&nbsp;</Stack.Item>
      </Stack> */}
    </Stack>
  );
};
