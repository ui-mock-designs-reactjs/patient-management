import { Stack, Text } from "@fluentui/react";
import React from "react";
import "./scans.scss";
import scan1 from '../../images/scan1.jpeg';
import scan2 from '../../images/scan2.webp';
import scan3 from '../../images/scan3.png';
import scan4 from '../../images/scan4.jpeg';

export const Scans = () => {
  return (
    <Stack className="scans-screen">
      <Text className="heading" variant="medium">
        Scans
      </Text>
      <Stack horizontal>
          <img src={scan1} className="scan-img" />
          <img src={scan2} className="scan-img" />
          <img src={scan3} className="scan-img" />
          <img src={scan4} className="scan-img" />
      </Stack>
    </Stack>
  );
};
