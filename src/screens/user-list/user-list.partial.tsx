import React from "react";
import { UserListItem } from "../../components/user-list-item/user-list-item.component";
import { IUser, SAMPLE_USERS } from "./sample-data";
import "./user-list.partial.scss";
import { SearchBox } from "@fluentui/react";

interface Props {
  setSelectedUser: Function;
  selectedUser?: IUser;
}

export const UserListing = (props: Props) => {
  return (
    <div className="user-list-container">
      <div className="search-container">
        <SearchBox
          placeholder="Search"
          onSearch={(newValue) => console.log("value is " + newValue)}
        />
      </div>
      <div className="user-list">
        {SAMPLE_USERS.map((user) => (
          <div
            key={user.id}
            className={`user-list-item ${
              props.selectedUser === user ? "is-selected" : ""
            }`}
            onClick={() => props.setSelectedUser(user)}
          >
            <UserListItem user={user} />
          </div>
        ))}
      </div>
    </div>
  );
};
