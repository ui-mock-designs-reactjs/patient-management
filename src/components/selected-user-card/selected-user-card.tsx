import React from "react";
import { IUser } from "../../screens/user-list/sample-data";
import "./selected-user-card.scss";
import {
  PersonaSize,
  Stack,
  DefaultButton,
  Text,
  VerticalDivider,
  IContextualMenuProps,
} from "@fluentui/react";
import { UserListItem } from "../user-list-item/user-list-item.component";

interface Props {
  user: IUser;
}

const menuProps: IContextualMenuProps = {
  items: [
    {
      key: "emailMessage",
      text: "Email",
      iconProps: { iconName: "Mail" },
    },
    {
      key: "calendarEvent",
      text: "WhatsApp",
      iconProps: { iconName: "Chat" },
    },
  ],
};

export const SelectedUserCard = (props: Props) => {
  return (
    <Stack horizontalAlign="center" className="selected-user-card">
      <UserListItem
        hidePresence={true}
        user={props.user}
        hidePersonaDetails={true}
        size={PersonaSize.size56}
      />
      <Text variant="large" className="user-name">
        {props.user.first_name} {props.user.last_name}
      </Text>
      <Text variant="small" className="user-email">
        {props.user.email}
      </Text>
      <Stack grow={1} className="appointments" horizontal>
        <Stack horizontalAlign="center" grow={1}>
          <div>
            <Text variant="large">{props.user.past_appointments}</Text>
          </div>
          <div>
            <Text variant="small">Recent</Text>
          </div>
        </Stack>
        <VerticalDivider />
        <Stack horizontalAlign="center" grow={1}>
          <div>
            <Text variant="large">{props.user.upcoming_appointments}</Text>
          </div>
          <div>
            <Text variant="small">Upcoming</Text>
          </div>
        </Stack>
      </Stack>
      <DefaultButton
        text="Send Message"
        split
        menuProps={menuProps}
        onClick={() => {}}
        checked={false}
      />
    </Stack>
  );
};
