import React, { useState } from "react";
import { IUser } from "../../screens/user-list/sample-data";
import { Persona, PersonaPresence, PersonaSize } from "@fluentui/react";
import { getRandomArbitraryInt } from "../../utility/getRandomArbitraryInt";
import "./user-list-item.component.scss";

interface Props {
  user: IUser;
  hidePresence?: boolean;
  size?: PersonaSize;
  hidePersonaDetails?: boolean;
}

const UserPresence = [
  PersonaPresence.away,
  PersonaPresence.blocked,
  PersonaPresence.busy,
  PersonaPresence.dnd,
  PersonaPresence.none,
  PersonaPresence.offline,
  PersonaPresence.online,
];

export const UserListItem = (props: Props) => {
  const imageUrl =
    props.user.gender === "Male"
      ? `https://randomuser.me/api/portraits/men/${props.user.id}.jpg`
      : `https://randomuser.me/api/portraits/women/${props.user.id}.jpg`;

  const randomPresenceIndex = getRandomArbitraryInt(0, UserPresence.length);
  const [presence] = useState<PersonaPresence>(
    UserPresence[randomPresenceIndex]
  );

  console.log("Presence: ", presence);

  const fullName = props.user.first_name + " " + props.user.last_name;
  const initials = fullName
    .split(" ")
    .map((word) => word[0])
    .join("");

  return (
    <Persona
      hidePersonaDetails={props.hidePersonaDetails ? true : false}
      presence={props.hidePresence ? PersonaPresence.none : presence}
      size={props.size ? props.size : PersonaSize.size48}
      imageUrl={imageUrl}
      imageInitials={initials}
      text={fullName}
      secondaryText={props.user.email}
    />
  );
};
