import React, { useState } from "react";
import "./detail-selector-tabs.scss";
import { PersonaSize, Stack, Text, VerticalDivider } from "@fluentui/react";

export const DetailSelectorTabs = () => {
  const [selectedTab, setSelectedTab] = useState("UPCOMING");
  return (
    <Stack className="detail-selector-tabs" horizontal>
      <Stack.Item
        onClick={() => setSelectedTab("UPCOMING")}
        className={`tab-item ${selectedTab === "UPCOMING" ? "active" : ""}`}
        grow={1}
      >
        Upcoming Appointments
      </Stack.Item>
      <Stack.Item
        onClick={() => setSelectedTab("PAST")}
        className={`tab-item ${selectedTab === "PAST" ? "active" : ""}`}
        grow={1}
      >
        Past Appointments
      </Stack.Item>
      <Stack.Item
        onClick={() => setSelectedTab("RECORDS")}
        className={`tab-item ${selectedTab === "RECORDS" ? "active" : ""}`}
        grow={1}
      >
        Medical Records
      </Stack.Item>
    </Stack>
  );
};
