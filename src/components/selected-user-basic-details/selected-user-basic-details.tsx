import React from "react";
import { IUser } from "../../screens/user-list/sample-data";
import "./selected-user-basic-details.scss";
import {
  PersonaSize,
  Stack,
  DefaultButton,
  Text,
  VerticalDivider,
} from "@fluentui/react";
import { UserListItem } from "../user-list-item/user-list-item.component";

interface Props {
  user: IUser;
}

export const SelectedUserBasicDetails = (props: Props) => {
  return (
    <Stack horizontal grow={1}>
      <Stack verticalAlign="space-around" grow={1} className="basic-details">
        <Stack horizontal>
          <Stack grow={1} horizontal>
            <Stack horizontalAlign="center" grow={1}>
              <div>
                <Text className="detail-label" variant="small">
                  Gender
                </Text>
              </div>
              <div>
                <Text variant="medium">{props.user.gender}</Text>
              </div>
            </Stack>
          </Stack>
        </Stack>
        <Stack horizontal>
          <Stack grow={1} horizontal>
            <Stack horizontalAlign="center" grow={1}>
              <div>
                <Text className="detail-label" variant="small">
                  Phone Number
                </Text>
              </div>
              <div>
                <Text variant="medium">{props.user.phone}</Text>
              </div>
            </Stack>
          </Stack>
        </Stack>
        <Stack horizontal>
          <Stack grow={1} horizontal>
            <Stack horizontalAlign="center" grow={1}>
              <div>
                <Text className="detail-label" variant="small">
                  Status
                </Text>
              </div>
              <div>
                <Text variant="medium">Active</Text>
              </div>
            </Stack>
          </Stack>
        </Stack>
      </Stack>
      <Stack
        verticalAlign="space-around"
        grow={1}
        className="basic-details"
        style={{ marginRight: 0 }}
      >
        <Stack horizontal>
          <Stack grow={1} horizontal>
            <Stack horizontalAlign="center" grow={1}>
              <div>
                <Text className="detail-label" variant="small">
                  Birthday
                </Text>
              </div>
              <div>
                <Text variant="medium">{props.user.dob}</Text>
              </div>
            </Stack>
          </Stack>
        </Stack>
        <Stack horizontal>
          <Stack grow={1} horizontal>
            <Stack horizontalAlign="center" grow={1}>
              <div>
                <Text className="detail-label" variant="small">
                  City
                </Text>
              </div>
              <div>
                <Text variant="medium">{props.user.city}</Text>
              </div>
            </Stack>
          </Stack>
        </Stack>
        <Stack horizontal>
          <Stack grow={1} horizontal>
            <Stack horizontalAlign="center" grow={1}>
              <div>
                <Text className="detail-label" variant="small">
                  Zipcode
                </Text>
              </div>
              <div>
                <Text variant="medium">{props.user.zipcode || 110045}</Text>
              </div>
            </Stack>
          </Stack>
        </Stack>
      </Stack>
    </Stack>
  );
};
